all: folge matrix

matrix: matrixMain.c
	gcc -std=c11 -Wall -Wextra -pedantic matrixMain.c matrix.c matrix.h aufgabe1copied.c aufgabe1copied.h -o matrix.out

folge: folge.c
	gcc -std=c11 folge.c folge.h aufgabe2copied.c aufgabe2copied.h -o folge.out -lm

run:
	./matrix.out