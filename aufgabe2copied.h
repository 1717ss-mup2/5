#ifndef AUFGABE2COPIED_H
#define AUFGABE2COPIED_H

#include <stdlib.h>
#include <string.h>

//copied aus aufgabe
typedef enum { bigEndian, littleEndian } Endianness;

double doubleSwap(const double value);
Endianness systemEndian();
double *endianDoubleSwap(double *result, const char *endian, const int elementCount);

#endif