/*
 * Author: Leonard Krause <kontakt@herr-ek.de>
 *
 */

#include "folge.h"

int main(int argc, char** argv) {
  for (unsigned int i=0; i<=10; i++){
    double test=recursive(i);
    double test2=implizit(i);
    printf("n=%i -> recursive: %f explizit: %f\n",i,test, test2);
  }

  double* content = readBinaryFile("exercise3b.bin");
  printf("reading completed!\n");
  int testResult;
  printf("tests (this can take some time... recursive is exponential -.-)\n");
  testResult = equalTest(content,(size_t)50);
  printf("testResult: %i",testResult);
}

// g(0)=0
// g(1)=1
// g(2)=1/2
// g(3)=3/4
// g(4)=5/8
// g(5)=11/16
// g(6)=21/32

double implizit(unsigned int n){
  return (2.0/3.0 * (1 - pow(-0.5, n)));
}

double recursiveOld(unsigned int n){
  if (n==0) return 0;
  if (n==1) return 1;
  double a = recursive(n-2);
  double b = recursive(n-1);
  return 0.5*(a+b);
}

double recursive(unsigned int n){
  if (n==0) return 0;
  if (n==1) return 1;
  double a = recursive(n-2);
  double b = recursive(n-1);
  return 0.5*(a+b);
}

FileHeader *readBinaryHeaderFile(const char *fileName){
  FileHeader *result = (FileHeader*)malloc(sizeof(FileHeader));
  FILE *exercise3b;
  exercise3b=fopen(fileName,"r");
	if (!exercise3b)
	{
		printf("Error! Couldnt open file");
		return NULL;
	}
	fread(result,sizeof(FileHeader),1,exercise3b);
  fclose(exercise3b);
  printf("\n#Header#\n");
  printf("\nfileName: %s\nendian: %s\ndataType: %s\nsizeOfDataType %s\nelementCount: %s\n",
    result->fileName,result->endian,result->dataType,result->sizeOfDataType,result->elementCount);
  return result;
}

double *readBinaryFile(const char *fileName){
  FILE *exercise3b = fopen(fileName,"r");
  if (!exercise3b)
  {
    printf("Error! Couldnt open file");
    return NULL;
  }
  FileHeader *header = readBinaryHeaderFile(fileName);
  int size, count;
  if(sscanf(header->sizeOfDataType,"%i",&size)!=1){
      printf("strange input file, abort");
      return NULL;
  }
  if(sscanf(header->elementCount,"%i",&count)!=1){
    printf("strange input file, abort");
      return NULL;
  }
  double *result = (double*)malloc(size*count);
  int flag=0;
  flag+=fseek(exercise3b,sizeof(FileHeader),SEEK_SET);
  flag+=fread(result,size,count,exercise3b);
  fclose(exercise3b);
  if(flag!=count){
    printf("some error while reading the file: %i",flag);
    return NULL;
  }
  result = endianDoubleSwap(result,header->endian,count);
  /*
  printf("\nreading %i of %i bytes\n",count,size);
  for (int i=0; i<=count-1;i++){
        printf("raed %i: %f\n",i,result[i]);
  }
  printf("reading done\n\n");
  */
  return result;
}

int equalTest(const double *content, const size_t len){
  for (size_t i=0; i<=len-1; i++){
    if(recursive(i) != content[i]) return i;
    printf("test %lu/%lu was successful\n",i+1,len);
  }
  return 0;
}

int equalTestexplizit(const double *content, const size_t len){
  for (size_t i=0; i<=len-1; i++){
    if(implizit(i) != content[i]) return i;
    printf("test %lu/%lu was successful\n",i+1,len);
  }
  return 0;
}