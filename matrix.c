/*
 * Author: Leonard Krause <kontakt@herr-ek.de>
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include "matrix.h"
#include <string.h>

/*
 *
 */
/**
 * Allocates space for a NxM matrix.
 *
 * Please note, each row must have the same number of columns.
 *
 * @note The elements are initialized with zero.
 *
 * @param rows Number of rows.
 * @param cols Number of columns.
 *
 * @return Pointer to the allocated matrix object.
 */
Matrix newMatrix( size_t rows, size_t cols ){
    if (rows <= 0 || cols <= 0) return NULL;
    Matrix matrix = (double**)calloc(rows,sizeof(double*));
    for (size_t i=0; i<=rows-1;i++){
      matrix[i]=(double*)calloc(cols,sizeof(double));
    }
    return matrix;
}
/**
 * Copies a given matrix into a new memory location.
 *
 * @param m Pointer to the matrix which should be copied.
 * @param rows Number of rows.
 * @param cols Number of columns.
 *
 * @return Pointer to the deep-copy of the matrix.
 */
Matrix copyMatrix( Matrix m, size_t rows, size_t cols ){
  Matrix copy = newMatrix(rows,cols);
  memcpy(copy, m, sizeof(double)*rows*cols);
  return copy;
}
/**
 * Destroys the given matrix containing N rows.
 *
 * @param m Pointer to the matrix which should be destroyed.
 * @param rows Number of rows.
 */
void deleteMatrix( Matrix m, size_t rows ){
  for (size_t i=0; i<=rows-1; i++){
    free(m[i]);
  }
  free(m);
}

/**
 * Renders a matrix to stdout.
 *
 * Each row is rendered in a single line where the matrix elements of the row
 * are separated by whitespace. The columns should be aligned in whitespace.
 * The row ends with a newline character.
 *
 * @param m Pointer to the matrix which should be printed to stdout.
 * @param rows Number of rows.
 * @param cols Number of columns.
 */
void printMatrix( Matrix m, size_t rows, size_t cols ){
  for (size_t i=0; i<=rows-1; i++){
    for (size_t k=0; k<=cols-1; k++){
      printf("%.2f ",m[i][k]);
    }
    printf("\n");
  }
}
/**
 * Multiplies two matrices A and B of dimensions A \in NxM and B \in MxP.
 *
 * @param a Pointer to first matrix.
 * @param b Pointer to second matrix.
 * @param n Number of rows of the first matrix.
 * @param m Number of columns of the first matrix and number of rows of the
 * second matrix.
 * @param p Number of columns of the second matrix.
 *
 * @return Pointer to the matrix product of A and B which is of dimension NxP.
 */
Matrix multMatrix( Matrix a, Matrix b, size_t n, size_t m, size_t p ){
  Matrix result = newMatrix(n,m);
  double temp;
  for(size_t aRows=0; aRows<=n-1; aRows++){
    for (size_t bCols=0; bCols<=p-1; bCols++){
      temp=0;
      for (size_t ab=0; ab<=m-1; ab++){
        //printf("a[%lu][%lu]=%f*b[%lu][%lu]=%f+",aRows,ab,a[aRows][ab],ab,bCols,b[ab][bCols]);
        temp += a[aRows][ab]*b[ab][bCols];
      }
      result[aRows][bCols]=temp;
    }
  }
  return result;
}
/**
 * Multiplies each matrix element with the given value.
 *
 * @note: This function alters the given matrix in place.
 *
 * @param a Pointer to the first matrix.
 * @param rows Number of rows.
 * @param cols Number of columns.
 * @param val Value for scaling.
 */
void scaleMatrix( Matrix a, size_t rows, size_t cols, double val ){
  for (size_t i=0; i<=rows-1; i++){
    for (size_t k=0; k<=cols-1; k++){
      a[i][k] *= val;
    }
  }
}

/**
 * Elementwise comparison of two matrices of same dimensions.
 *
 * @param a Pointer to first matrix.
 * @param b Pointer to the second matrix.
 * @param rows Number of rows.
 * @param cols Number of columns.
 *
 * @return True if the matrices are elementwisely the same, false otherwise.
 */
bool equalMatrix( Matrix a, Matrix b, size_t rows, size_t cols )
{
  for (size_t i=0; i<=rows-1; i++){
    for (size_t k=0; k<=cols-1; k++){
      if (a[i][k]!=b[i][k]) return false;
    }
  }
  return true;
}

char* toStringMatrix( Matrix m, size_t rows, size_t cols ){
  char* result = NULL;
  for (size_t x=0; x<=rows-1;x++){
    for (size_t y=0; y<=cols-1;y++){
      //snprintf returns the number of bytes which would have been written if tere was enough space
      int len = 2+snprintf(NULL,0,"%g",m[x][y]); //add 1 for the null teminator and 1 for a space
      char* temp = malloc(len);
      temp[0]=' ';
      snprintf(&temp[1],len,"%g",m[x][y]);
      result = concat(result,temp);
      if (result==NULL) return NULL;
    }
    result = concat(result,"\n");
    if (result==NULL) return NULL;
  }
  return result;
}

char* concat(const char *str1, const char *str2)
{
  if (str1==NULL && str2==NULL) return NULL;
  if (str1==NULL) return (char*)str2;
  if (str2==NULL) return (char*)str1;
  char *result = malloc(strlen(str1)+strlen(str2)+1); //space for both strings + null terminator
  if (result==NULL){
    printf("This shouldnt have happend! free some space? -> abort");
    return NULL;
  }
  strcpy(result, str1);
  strcat(result, str2);
  return result;
}