#include "aufgabe2copied.h"

//copied aus aufgabe

double doubleSwap(const double value)
{
  union {
    double value;
    unsigned char byte[8];
  } endian1, endian2;
  endian1.value = value;
  for (size_t i = 0; i < 8; ++i) {
  endian2.byte[i] = endian1.byte[7 - i];
  }
  return endian2.value;
}
Endianness systemEndian()
{
  int x = 1;
  if (*((char *)&x) == 1) {
  return littleEndian;
  }
  return bigEndian;
}

double *endianDoubleSwap(double *result, const char *endian, const int elementCount)
{
  Endianness fileEndian =
  strcmp("big endian", endian) == 0 ? bigEndian : littleEndian;
  if (systemEndian() != fileEndian) {
  for (int i = 0; i < elementCount; ++i, ++result) {
  *result = doubleSwap(*result);
  // printf("%2zu %.23f\n", i, *result);
  }
  result -= elementCount;
  }
  return result;
}