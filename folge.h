#ifndef FOLGE_H
#define FOLGE_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "aufgabe2copied.h"

typedef struct {
  char fileName[60];
  char endian[20]; // either "big endian" or "little endian"
  char dataType[20];
  char sizeOfDataType[10];
  char elementCount[10];
} FileHeader;

double recursive(unsigned int n);
double implizit(unsigned int n);
FileHeader *readBinaryHeaderFile(const char *fileName);
double *readBinaryFile(const char *fileName);
int equalTest(const double *content, const size_t len);
//returns nr of duble which failed the test or 0 if test completed successful
double *endianDoubleSwap(double *result, const char *endian, const int elementCount);

#endif