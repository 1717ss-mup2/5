#include "aufgabe1copied.h"

//copied aus aufgabe
// includes needed: inttypes.h stdlib.h time.h
double randomDouble() {
RndPattern result;
// RAND_MAX is 7FFFFFFF hence only 31 bits randomized, leaving the signum Modellierung u. Programmierung 2 – SS17 https://moodle2.uni-leipzig.de/course/view.php?id=13412
// always positive instead we take only the last two bytes and paste them
// into the memory
for( size_t i = 0; i < sizeof( double ) / 2; ++i ) {
result.ull = result.ull << 16 | ( rand() % 65536 );
}
return result.dbl;
}
Matrix randomMatrix( size_t rows, size_t cols ) {
srand(time(0));
Matrix m = newMatrix( rows, cols );
if( m ) {
for( size_t i = 0; i < rows; ++i ) {
if( m[i] ) {
for( size_t j = 0; j < cols; ++j ){
m[i][j] = randomDouble();
}
}
}
}
return m;
}