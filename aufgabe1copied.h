#ifndef AUFGABE1COPIED_H
#define AUFGABE1COPIED_H

#include <inttypes.h>
#include <stdlib.h>
#include <time.h>
#include "matrix.h"

// includes needed: inttypes.h stdlib.h time.h
typedef union { double dbl; uint64_t ull; } RndPattern;
double randomDouble();
Matrix randomMatrix( size_t rows, size_t cols );

#endif